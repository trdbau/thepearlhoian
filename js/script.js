jQuery(function () {
	'use strict';
	/*faq pagination js*/
	function toggleIcon(e) {
		$(e.target)
			.prev('.panel-heading')
			.find('.more-less')
			.toggleClass('glyphicon-plus glyphicon-minus');
	}
	$('.panel-group').on('hidden.bs.collapse', toggleIcon);
	$('.panel-group').on('shown.bs.collapse', toggleIcon);
	/*end-faq pagination js*/

	/*onepage menu scroll js*/

	jQuery('.scroll').on('click', function (event) {
		event.preventDefault();
		var hash = this.hash;
		jQuery('html, body').animate({
				scrollTop: jQuery(hash).offset().top - 75
			},
			1000
		);
	});

	/*end-onepage menu scroll js*/

	/*onepage menu active on scroll js*/
	$(document).on('scroll', onScroll);

	function onScroll(event) {
		var scrollPos = $(document).scrollTop();
		$('.navbar-nav li.menu .scroll').each(function () {
			var currLink = $(this);
			var refElement = $(currLink.attr('href'));
			if (
				refElement.position().top - 105 <= scrollPos &&
				refElement.position().top + refElement.height() > scrollPos
			) {
				$('.navbar-nav li.menu .scroll').removeClass('active');
				currLink.addClass('active');
			} else {
				currLink.removeClass('active');
			}
		});
	}
	/*end-onepage menu active on scroll js*/

	/*mobile collapse menu close js*/
	$(document).on('click.nav', '.navbar-collapse.in', function (e) {
		if (
			$(e.target).is('.navbar-nav li.menu .scroll') ||
			$(e.target).is('button')
		) {
			$(this).collapse('hide');
		}
	});
	/*end-mobile collapse menu close js*/

	/*sticky header js*/
	jQuery(window).on('scroll', function () {
		if (jQuery(window).scrollTop() === 0) {
			jQuery('.nexa-header-business .navbar.navbar-default').removeClass(
				'sticky-header'
			);
			jQuery('.nexa-header-business').removeClass('sticky-header');
		} else {
			jQuery('.nexa-header-business .navbar.navbar-default').addClass(
				'sticky-header'
			);
			jQuery('.nexa-header-business').addClass('sticky-header');
		}
	});
	/*end-sticky header js*/
	$('.gallery').pignoseGallery({
		thumbnails: '.gallery-thumbnails'
	});
	$('.menu .item').tab();
});

$(' .popup__content i').click(function () {
	$('body').css('overflow', 'auto');
	$('.popup').hide();
});

$(document).ready(function () {
	var submit1 = $('form#googleForm1');
	submit1.submit(function (event) {
		let data = $('form#googleForm1').serialize();
		$.ajax({
			type: 'POST',
			url: 'https://script.google.com/macros/s/AKfycbyZye3Ctf5LAYW1oKMIpsyIP9VxohTOMvyL3DGZGmuYufmej4OG/exec',
			dataType: 'json',
			crossDomain: true,
			data: data,
			success: function (data) {
				if (data == 'false') {
					alert('Xãy ra lỗi! Vui lòng thử lại.');
				} else {
					alert('Đăng ký thành công!');
				}
			}
		});
	});

	var submit2 = $('#submitForm2');
	submit2.submit(function () {
		let data = $('form#googleForm2').serialize();
		$.ajax({
			type: 'POST',
			url: 'https://script.google.com/macros/s/AKfycbyZye3Ctf5LAYW1oKMIpsyIP9VxohTOMvyL3DGZGmuYufmej4OG/exec',
			dataType: 'json',
			crossDomain: true,
			data: data,
			success: function (data) {
				if (data == 'false') {
					alert('Xãy ra lỗi! Vui lòng thử lại.');
				} else {
					alert('Đăng ký thành công!');
				}
			}
		});
		return false;
	});

	var submit3 = $('#submitForm3');
	submit3.submit(function () {
		let data = $('form#googleForm3').serialize();
		$.ajax({
			type: 'POST',
			url: 'https://script.google.com/macros/s/AKfycbyZye3Ctf5LAYW1oKMIpsyIP9VxohTOMvyL3DGZGmuYufmej4OG/exec',
			dataType: 'json',
			crossDomain: true,
			data: data,
			success: function (data) {
				if (data == 'false') {
					alert('Xãy ra lỗi! Vui lòng thử lại.');
				} else {
					alert('Đăng ký thành công!');
				}
			}
		});
		return false;
	});

	var submit4 = $('#submitForm4');
	submit4.submit(function () {
		let data = $('form#googleForm4').serialize();
		$.ajax({
			type: 'POST',
			url: 'https://script.google.com/macros/s/AKfycbyZye3Ctf5LAYW1oKMIpsyIP9VxohTOMvyL3DGZGmuYufmej4OG/exec',
			dataType: 'json',
			crossDomain: true,
			data: data,
			success: function (data) {
				if (data == 'false') {
					alert('Xãy ra lỗi! Vui lòng thử lại.');
				} else {
					alert('Đăng ký thành công!');
				}
			}
		});
		return false;
	});
});